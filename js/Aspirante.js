function LoadAspirante() {
    var serviceUrl = "";
  
    $(document).ready(function () {
        $.ajax({
            url: "http://192.168.100.78:4000/api/aspirantesapi",
            type: 'GET',
            dataType: 'json',
            headers: { 'content-type': 'application/json;charset=utf-8' },
            beforeSend: function () {
                $("#ImageAspirante").attr('src', "img/fluid-loader.gif");
            },
            success: function (data) {
                $("#ImageAspirante").attr('src', 'img/aspirantes.jpg');
                $("#TitleAsp").append('Aspirantes');
                $("#DescriptionAsp").append('Lista de los ultimos aspirantes agregados');
                
                $("#FooterAsp").append("Estatus : Activo.");
                $("#Asp").removeClass("invisible")
                if (data !== null) {
                    $.each(data, function (key, value) {
                        var rows = "<tr>"
                            + "<td>" + value.nombre + "</td>"
                            + "<td>" + value.apellidoPaterno + "</td>"
                            + "<td>" + value.apellidoMaterno + "</td>"
                            + "</tr>";
                        $('#Asp tbody').append(rows);
                    });
                }
                $('#ASPLINK').append('<a class="btn btn-outline-success" target="_blank" href="http://192.168.100.78:4000/Account/Login" role="button">Inicio</a>');


            },
            error: function (response) {
               
                $("#FooterAsp").append("Estatus : Desactivado.");
                $("#TitleAsp").append('Error 404');
                $("#ImageAspirante").attr('src', "img/error_rana.gif");
            }
        });
    });
}
