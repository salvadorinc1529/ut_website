var myapp = angular.module('app', []);

myapp.controller('homeController', function ($scope, $http) {

    $scope.data = null;
    $scope.error = {};
    var url = "http://192.168.100.95:3000/";


    function init() {
        $http.get(url)
            .then((data, error) => {
                $scope.error.status = data.status;
                $scope.error.statusText = data.statusText;
                $scope.data = data.data;
            }, (error) => {
                if(error){
                    $scope.error.status = error.status;
                    $scope.error.statusText = error.statusText;
                }
                $scope.data = error.data;
            })
    }

    init();
    $scope.initEdit = function () {
        if($scope.data.activo){
            location.href = url + "login";
        }
    };

});