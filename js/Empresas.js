function LoadEnterprisesData() {
    var serviceUrl = "http://www.webapijadk.com/api/Enterprises";
    var enterprisesSiteUrl = "http://www.empwebjadk.com";
    $.ajax({
        url: serviceUrl,
        method: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $("#Image").attr('src', "img/loaderemp.gif");
        },
        success: function (data) {
            if (data !== null) {
                $.each(data, function (key, value) {
                    $("#Image").attr('src', value.Image);
                    $("#Title").append(value.Title);
                    $("#Description").append(value.Description);
                    $("#EnterprisesHref").attr('href', enterprisesSiteUrl);
                    $("#EnterprisesHrefLink").attr('href', enterprisesSiteUrl);
                    $("#Footer").append("Datos cargados correctamente.");
                });
            }
        },
        error: function (response) {
            $("#EnterprisesHref").attr('href', enterprisesSiteUrl);
            $("#EnterprisesHrefLink").attr('href', enterprisesSiteUrl);
            $("#Footer").append("Algo mal ocurrió, por favor intentalo de nuevo.");
            $("#Image").attr('src', "img/erroremp.gif");
        }
    });
}

